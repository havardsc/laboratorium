﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Ephere.Graphics;
using System.Windows.Media;

namespace HavardscLaboratorium.Camogenerator.Model
{
    public class KulerColor
    {
        static Random random = new Random();

        public string Name { get; private set; }
        //public int ID { get; private set; }
        public string PreviewPath { get; private set; }
        public Color4b[] Colors { get; private set; }

        public KulerColor(string name, string previewPath, Color4b[] colors)
        {
            Name = name;
            PreviewPath = previewPath;
            Colors = colors;
        }

        static KulerColor FromXmlNode(XmlNode node)
        {
            string name = node.ChildNodes[0].InnerText.Split(':')[1].Trim();
            string previewPath = node.ChildNodes[3].InnerText.Substring(
                node.ChildNodes[3].InnerText.IndexOf("https://")).Trim();
            string[] sColors = node.ChildNodes[4].InnerText.Substring(
                node.ChildNodes[4].InnerText.IndexOf("Hex:")).Split('\n').Where(
                i => !string.IsNullOrWhiteSpace(i)).Select(
                j => "#" + j.Trim().Replace(",", "")).Skip(1).ToArray();

            List<Color4b> colors = new List<Color4b>();
            foreach (string sc in sColors)
            {
                Color c = (Color)ColorConverter.ConvertFromString(sc);
                colors.Add(new Color4b(c.R, c.G, c.B));
            }

            return new KulerColor(name, previewPath, colors.ToArray());
        }

        public static List<KulerColor> GetRandomKulerColors()
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.Load(@"http://kuler-api.adobe.com/rss/get.cfm?listtype=random&itemsPerPage=10&timespan=" + random.Next(1000).ToString() + "&key=INSERTYOURKULERKEYHERE");
            XmlNamespaceManager xnm = new XmlNamespaceManager(xdoc.NameTable);
            xnm.AddNamespace("xs", "http://www.w3.org/2001/XMLSchema");
            xnm.AddNamespace("kuler", "http://kuler.adobe.com/kuler/API/rss/");
            xnm.AddNamespace("rss", "http://blogs.law.harvard.edu/tech/rss");

            XmlNodeList nodes = xdoc.SelectNodes("//item", xnm);
            
            List<KulerColor> colors = new List<KulerColor>();
            foreach (XmlNode node in nodes)
                colors.Add(KulerColor.FromXmlNode(node));

            return colors;
        }
    }
}
