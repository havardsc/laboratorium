﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ephere.Geometry;
using Ephere.Graphics;

namespace HavardscLaboratorium.Camogenerator.Model
{
    public static class Camogenerator
    {
        public static Color4b[] CreateMap(int size,
            IEnumerable<Color4b> colorsIE, int iterations, int numPoints, int brushSize, int brushDelta, int blurSize, int seed)
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            Color4b[] colors = colorsIE.ToArray(); // Remove this line
            var random = new Random(seed);
            var sizeSq = size * size;
            var initColor = new Color4b(colors[0]);
            var colorGrid = new Color4b[sizeSq];

            for (int i = 0; i < sizeSq; i++)
                colorGrid[i] = initColor;

            var points = Enumerable.Range(0, numPoints).Select(
                x => new Vector2(random.Next(size), random.Next(size))).ToArray();

            var brushOffset = brushSize / 2;

            for (int i = 0; i < iterations; i++)
                for (int j = 0; j < points.Length; j++)
                {
                    var point = points[j];
                    var dPx = random.NextDouble() * 2 - 1;
                    var dPy = random.NextDouble() * 2 - 1;
                    var length = Math.Sqrt(Math.Pow(dPx, 2) + Math.Pow(dPy, 2));
                    dPx *= brushDelta / length;
                    dPy *= brushDelta / length;
                    point.x = (float)Math.Max(0, Math.Min(size - 1, point.x + dPx));
                    point.y = (float)Math.Max(0, Math.Min(size - 1, point.y + dPy));

                    var x0 = (int)Math.Max(0, point.x - brushOffset);
                    var y0 = (int)Math.Max(0, point.y - brushOffset);
                    var x1 = (int)Math.Min(size - 1, point.x + brushOffset);
                    var y1 = (int)Math.Min(size - 1, point.y + brushOffset);
                    points[j] = point;

                    for (int y = y0; y < y1; y++)
                        for (int x = x0; x < x1; x++)
                            colorGrid[x + y * size] = colors[j % colors.Length];//new Color4b(color);
                }

            if (blurSize == 0)
                return colorGrid;

            var blurredMap = new Color4b[colorGrid.Length];

            for (int y = 0; y < size; y++)
                for (int x = 0; x < size; x++)
                {
                    int r = 0, g = 0, b = 0;
                    var pCount = 0;

                    var x0 = (int)Math.Max(0, x - blurSize);
                    var y0 = (int)Math.Max(0, y - blurSize);
                    var x1 = (int)Math.Min(size - 1, x + blurSize);
                    var y1 = (int)Math.Min(size - 1, y + blurSize);

                    for (int i = y0; i <= y1; i++)
                        for (int j = x0; j <= x1; j++)
                        {
                            var c = colorGrid[j + i * size];
                            r += c.RedValue; g += c.GreenValue; b += c.BlueValue;
                            pCount++;
                        }
                    r /= pCount; g /= pCount; b /= pCount;
                    blurredMap[x + y * size] = new Color4b((byte)r, (byte)g, (byte)b);
                }
            sw.Stop();
            Autodesk.Max.GlobalInterface.Instance.TheListener.EditStream.Printf(
                    string.Format("Finished creating map. Time: {0}\n", sw.ElapsedMilliseconds));

            return blurredMap;
        }

        public static Color4b GetColor(float x, float y, Color4b[] colormap, int size)
        {
            //if (x < 0 || x > 1 || y < 0 || y > 1)
            //    GlobalInterface.Instance.TheListener.EditStream.Printf(
            //        string.Format("Input values: {0} {1}", x, y));
            return colormap[(int)(x * (size - 1)) + (int)(y * (size - 1)) * size];
        }

    }
}
