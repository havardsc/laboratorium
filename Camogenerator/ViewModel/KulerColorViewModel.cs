﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using HavardscLaboratorium.Camogenerator.Model;

namespace HavardscLaboratorium.Camogenerator.ViewModel
{
    public class KulerColorViewModel : ViewModelBase
    {
        public KulerColor Color { get; private set; }

        public string Thumbnail { get { return Color.PreviewPath; } }

        public KulerColorViewModel(KulerColor color)
        {
            this.Color = color;
            DisplayName = color.Name;
        }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
