﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.ComponentModel;
using Ephere.Graphics;

namespace HavardscLaboratorium.Camogenerator.ViewModel
{
    public class ColorPickerViewModel : ViewModelBase
    {
        BackgroundWorker bgWorker;
        IEnumerable<KulerColorViewModel> workerResults;

        List<KulerColorViewModel> randomColors;
        public List<KulerColorViewModel> RandomColors 
        {
            get { return randomColors; }
            set
            {
                randomColors = value;
                OnPropertyChanged("RandomColors");
            }
        }

        KulerColorViewModel selectedColor;
        public KulerColorViewModel SelectedColor
        {
            get { return selectedColor; }
            set
            {
                selectedColor = value;

                if (selectedColor != null && !string.IsNullOrEmpty(MaxScriptToExecuteOnColorSelected))
                {
                    Color4b[] colors = selectedColor.Color.Colors;
                    Color4b c0 = colors[0];
                    Color4b c1 = colors[1];
                    Color4b c2 = colors[2];
                    Color4b c3 = colors[3];
                    Color4b c4 = colors[4];

                    Autodesk.Max.GlobalInterface.Instance.ExecuteMAXScriptScript(
                        string.Format(MaxScriptToExecuteOnColorSelected, 
                        c0.RedValue, c0.GreenValue, c0.BlueValue,
                        c1.RedValue, c1.GreenValue, c1.BlueValue,
                        c2.RedValue, c2.GreenValue, c2.BlueValue,
                        c3.RedValue, c3.GreenValue, c3.BlueValue,
                        c4.RedValue, c4.GreenValue, c4.BlueValue),
                        false,
                        null);

                }
                OnPropertyChanged("SelectedColor");
            }
        }

        bool isLoadingRandomColors;
        public bool IsLoadingRandomColors
        {
            get { return isLoadingRandomColors; }
            set
            {
                isLoadingRandomColors = value;
                OnPropertyChanged("IsLoadingRandomColors");
            }
        }

        public string MaxScriptToExecuteOnColorSelected { get; set; }

        public ICommand UpdateRandomColorsCommand { get; private set; }

        public ColorPickerViewModel()
        {
            RandomColors = new List<KulerColorViewModel>();
            bgWorker = new BackgroundWorker();
            bgWorker.DoWork += bgWorker_DoWork;
            bgWorker.RunWorkerCompleted += bgWorker_RunWorkerCompleted;

            UpdateRandomColorsCommand = new RelayCommand<object>(UpdateRandomColors);//, _ => !IsLoadingRandomColors);
            UpdateRandomColors(null);
        }

        //public void OnColorSelected()
        //{
        //    EventHandler handler = ColorSelected;
        //    Autodesk.Max.GlobalInterface.Instance.TheListener.EditStream.Printf("Handler: " + handler.ToString() + "\n");
        //    if (handler != null)
        //    {
        //        foreach (Delegate del in handler.GetInvocationList())
        //        {
        //            Autodesk.Max.GlobalInterface.Instance.TheListener.EditStream.Printf(del.ToString() + "\n");
        //        }
        //        Autodesk.Max.GlobalInterface.Instance.TheListener.EditStream.Printf("Firing!\n");
        //        handler(this, EventArgs.Empty);
        //    }
        //}

        void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            IsLoadingRandomColors = false;
            RandomColors = new List<KulerColorViewModel>(workerResults);
        }

        void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            workerResults = Model.KulerColor.GetRandomKulerColors().Select(i => new KulerColorViewModel(i));
        }

        void UpdateRandomColors(object _)
        {
            IsLoadingRandomColors = true;
            bgWorker.RunWorkerAsync();
        }
    }
}
