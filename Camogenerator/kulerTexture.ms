(
	struct kulerCamoStruct 
	(
		materialIndex,
		texture,
		graph,
		custAttDef, 
		
		fn GetCoden =
		(
			dotNetClass "Ephere.Plugins.Coden.Global"
		),

		fn GetLibrary =
		(
			(GetCoden()).Library
		),

		fn GetGraph refTarget =
		(
			testPlugin = (GetCoden()).GetPlugin(Coden_getPluginID refTarget)
			testPlugin.GetGraph(Coden_getInstanceID refTarget)
		),

		fn SetGraph refTarget graph =
		(
			testPlugin = (GetCoden()).GetPlugin(Coden_getPluginID refTarget)
			testPlugin.SetGraph (Coden_getInstanceID refTarget) graph
		),

		fn CreateGraphFromXmlTemplateString xml =
		(
			library = GetLibrary()
			ext = dotNetClass "Ephere.Modeling.Dependency.GraphExtensions"
			ext.CreateGraphFromXmlTemplateString library xml
		),
		
		fn SetColor colorName r g b =
		(
			colorNode = graph.GetNodeByName colorName
			if colorNode != undefined then 
			(
				(colorNode.GetInput "RedValue").value = dotNetObject "System.Byte" r
				(colorNode.GetInput "GreenValue").value = dotNetObject "System.Byte" g
				(colorNode.GetInput "BlueValue").value = dotNetObject "System.Byte" b
			)	
		),

		fn CreateCustomAttirubtesDef = 
		(
			if custAttDef == undefined then
			(
				custAttDef = attributes kulerGUIData
				(
					local materialIndex
					local parent
					local initialize
					local guiRollout
					local dataContext
					local view
					
					fn setMapSize size =
					(
						if parent != undefined then
						(
							mapNode = parent.graph.GetNodeByName "CreateMap"
							if mapNode != undefined then 
								(mapNode.GetInput "size").value = size						
							
							getColorNode = parent.graph.GetNodeByName "GetColor"
							if getColorNode != undefined then 
								(getColorNode.GetInput "size").value = size						
						)
					)
					
					fn setIterations iterations =
					(
						if parent != undefined then
						(
							mapNode = parent.graph.GetNodeByName "CreateMap"
							if mapNode != undefined then 
								(mapNode.GetInput "iterations").value = iterations
						)
					)
					
					fn setNumPoints pointCount =
					(
						if parent != undefined then 
						(
							mapNode = parent.graph.GetNodeByName "CreateMap"
							if mapNode != undefined then
								(mapNode.GetInput "numPoints").value = pointCount
						)
					)
					
					fn setBrushSize size =
					(
						if parent != undefined then 
						(
							mapNode = parent.graph.GetNodeByName "CreateMap"
							if mapNode != undefined then
								(mapNode.GetInput "brushSize").value = size
						)
					)
					
					fn setBrushDelta delta=
					(
						if parent != undefined then 
						(
							mapNode = parent.graph.GetNodeByName "CreateMap"
							if mapNode != undefined then
								(mapNode.GetInput "brushDelta").value = delta
						)
					)
					
					fn setBlur amount =
					(
						if parent != undefined then 
						(
							mapNode = parent.graph.GetNodeByName "CreateMap"
							if mapNode != undefined then
								(mapNode.GetInput "blurSize").value = amount
						)
					)
					
					fn setSeedValue val =
					(
						if parent != undefined then 
						(
							mapNode = parent.graph.GetNodeByName "CreateMap"
							if mapNode != undefined then
								(mapNode.GetInput "seed").value = val
						)
					)
					
					fn initialize owner index =
					(
						parent = owner
						setMapSize guiRollout.sizeSpinner.value
						setIterations guiRollout.iterationsSpinner.value
						setNumPoints guiRollout.numPointsSpinner.value
						setBrushSize guiRollout.brushSizeSpinner.value
						setBrushDelta guiRollout.brushDeltaSpinner.value
						setBlur guiRollout.blurSpinner.value
						setSeedValue guiRollout.seedSpinner.value
						
						dataContext.MaxScriptToExecuteOnColorSelected = "(
							atts = custAttributes.get meditMaterials[" + (index as string) + "] 1
							atts.guirollout.owner.parent.setcolor \"Color0\" {0} {1} {2}
							atts.guirollout.owner.parent.setcolor \"Color1\" {3} {4} {5}
							atts.guirollout.owner.parent.setcolor \"Color2\" {6} {7} {8}
							atts.guirollout.owner.parent.setcolor \"Color3\" {9} {10} {11}
							atts.guirollout.owner.parent.setcolor \"Color4\" {12} {13} {14}
						)"
					)
					
					rollout guiRollout "Color parameters"
					(		
						spinner sizeSpinner "Map size:    " range:[32, 8192, 512] type:#integer align:#left width:80 across:2
						spinner iterationsSpinner "Iterations:" range:[0, 1e6, 1000] type:#integer align:#left width:80
						spinner numPointsSpinner "Num points:" range:[0, 1e6, 50] type:#integer align:#left width:80 across:2
						spinner brushSizeSpinner "Brush size:" range:[0, 1e5, 20] type:#integer align:#left width:80 
						spinner brushDeltaSpinner "Brush delta:" range:[0, 1e5, 10] type:#integer align:#left width:80 across:2
						spinner blurSpinner "Brush delta:" range:[0, 10, 1] type:#integer align:#left width:80 
						spinner seedSpinner "Seed:" range:[0, 1e6, 1234] type:#integer align:#left width:80
						
						dotNetControl elHost "System.Windows.Forms.Integration.ElementHost" height:250 width:300 
						
						on sizeSpinner changed val do
							setMapSize val
						
						on iterationsSpinner changed val do
							setIterations val
						
						on numPointsSpinner changed val do
							setNumPoints val
						
						on brushSizeSpinner changed val do
							setBrushSize val
						
						on brushDeltaSpinner changed val do
							setBrushDelta val
						
						on blurSpinner changed val do
							setBlur val
						
						on seedSpinner changed val do
							setSeedValue val
						
						on guiRollout open do 
						(
							--if view == undefined then 
								view = dotNetObject "HavardscLaboratorium.Camogenerator.View.ColorPickerView" 
							if dataContext == undefined then 
								dataContext = dotNetObject "HavardscLaboratorium.Camogenerator.ViewModel.ColorPickerViewModel"
								
							dotNet.SetLifeTimeControl dataContext #dotnet
							dotNet.SetLifeTimeControl view #dotnet
							
							view.Height = 350 
							view.Width = 350 
							
							view.DataContext = dataContext
							
							elhost.backColor = elhost.backColor.FromARGB 68 68 68
							elhost.Child = view
							
						)
					)
				)
				custAttributes.add meditMaterials[materialIndex] custAttDef
				(custAttributes.get meditmaterials[materialIndex] custAttDef).Initialize this materialIndex
			)
		),
		
		fn SetInitialGraph =
		(
			graph = CreateGraphFromXmlTemplateString "<?xml version=\"1.0\" encoding=\"utf-8\"?>
	<Templates xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.ephere.com/modeling/dependency/graph/templates.xsd\">
		<Graph Type=\"KulerCamoTexure\">
			<Tags>plugin</Tags>
			<Input Type=\"vector2\" Name=\"uvCoordinate\" Tags=\"surfaceSpace\" />
			<Output Type=\"color4b\" Name=\"myOutput\" />
			<Node Type=\"Ephere.Graphics.Color4b\" Name=\"Color0\"/>
			<Node Type=\"Ephere.Graphics.Color4b\" Name=\"Color1\"/>
			<Node Type=\"Ephere.Graphics.Color4b\" Name=\"Color2\"/>
			<Node Type=\"Ephere.Graphics.Color4b\" Name=\"Color3\"/>
			<Node Type=\"Ephere.Graphics.Color4b\" Name=\"Color4\"/>
			<Node Type=\"HavardscLaboratorium.Camogenerator.Model.Camogenerator.CreateMap\" Name=\"CreateMap\">
				<Parameter Name=\"colorsIE\">
					<ArrayValues>
						<string>[0,0,0,0]</string>
						<string>[0,0,0,0]</string>
						<string>[0,0,0,0]</string>
						<string>[0,0,0,0]</string>
						<string>[0,0,0,0]</string>
					</ArrayValues>
				</Parameter>
			</Node>
			<Node Type=\"HavardscLaboratorium.Camogenerator.Model.Camogenerator.GetColor\" Name=\"GetColor\">
				<Parameters>
					<Parameter Name=\"size\" Value=\"0\" Type=\"System.Int32\" />
				</Parameters>
			</Node>
			<Node Type=\"Ephere.Modeling.TypeValue\" Name=\"uvCoordinate\" />
			<Connection To=\"CreateMap\" Input=\"colorsIE\">
				<ElementConnections>
					<ElementConnection From=\"Color0\" Output=\"Itself\" />
					<ElementConnection From=\"Color1\" Output=\"Itself\" />
					<ElementConnection From=\"Color2\" Output=\"Itself\" />
					<ElementConnection From=\"Color3\" Output=\"Itself\" />
					<ElementConnection From=\"Color4\" Output=\"Itself\" />
				</ElementConnections>
			</Connection>
			<Connection From=\"CreateMap\" Output=\"Output\" Input=\"myOutput\"/>
			<Connection From=\"uvCoordinate\" Output=\"x\" To=\"GetColor\" Input=\"x\" />
			<Connection From=\"uvCoordinate\" Output=\"y\" To=\"GetColor\" Input=\"y\" />
			<Connection From=\"CreateMap\" Output=\"Output\" To=\"GetColor\" Input=\"colormap\" />
			<Connection Output=\"uvCoordinate\" To=\"uvCoordinate\" Input=\"TypeInstance\" />
			<Connection From=\"GetColor\" Output=\"Output\" Input=\"myOutput\" />
		</Graph>
	</Templates>"
			SetGraph texture graph
		), 
		
		on Create do
		(
			actionMan.executeAction 0 "50048"  
			if materialIndex == undefined then
				throw "The material index needs to be specified."
			dotNet.loadAssembly @"C:\Users\h�vard\AppData\Local\Ephere\Library\bin\HavardscLaboratorium.dll"
			
			texture = meditMaterials[materialIndex] = LabTexture()
			SetInitialGraph()
			CreateCustomAttirubtesDef()
			
			SetColor "Color0" 255 255 0
			SetColor "Color1" 0 0 255
			SetColor "Color2" 0 255 0
			SetColor "Color3" 255 0 0
			SetColor "Color4" 0 0 0
		) 
	)
	
	kulerCamoStruct materialIndex:1
)