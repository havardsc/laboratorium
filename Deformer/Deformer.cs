﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Autodesk.Max;
using Ephere.Geometry;

namespace HavardscLaboratorium.Deformer
{
    public class Deformer : MeshDefaults
    {
        IGlobal global;
        IInterface13 _interface;

        bool[] protectedVerts;




        public Deformer(float startTime)
            : base(startTime)
        {
            global = Autodesk.Max.GlobalInterface.Instance;
            _interface = global.COREInterface13;
        }

        public PolygonMesh3 Update(
            float time, 
            PolygonMesh3 collisionMesh,
            Xform3 baseMeshXform, 
            Xform3 collisionMeshXform, 
            float collisionOffsetPercentage,
            float returnSpeed,
            float volumeAmplifier,
            float bulgeMaxHeight,
            float bulgeDistance,
            float collisionDistanceLimit)
        {
            if (time > StartTime && time < CurrentTime || protectedVerts == null)
            {
                protectedVerts = new bool[CurrentMesh.Vertices.Count];
                return CurrentMesh;
            }
            //if (protectedVerts == null)
            //    protectedVerts = new bool[OriginalMesh.Vertices.Count];

            //global.TheListener.EditStream.Printf("Protected vertices: " + protectedVerts.Count(x => x) + "\n");

            CurrentTime = time;

            TriangleMesh3 triColMesh = new TriangleMesh3(collisionMesh);
            IEnumerable<Vector3> transformedVerts = triColMesh.Vertices.Select(x => baseMeshXform.Inverse * (collisionMeshXform * x));
            TriangleMesh3 triColMeshTransformed = new TriangleMesh3(transformedVerts, triColMesh.IndexTriangles);
            ISplittableVolume colVolHierarchy = triColMeshTransformed.GetVolumeHierarchy(5, 5);
            Vector3[] vertices = new Vector3[CurrentMesh.Vertices.Count];

            float cumulativeDistance = 0f;
            List<int> noColIndices = new List<int>();
            List<Vector3> colVecs = new List<Vector3>();

            for (int i = 0; i < CurrentMesh.Vertices.Count; i++)
            {
                Vector3 vert = CurrentMesh.Vertices[i];
                Vector3 altVert = vert;
                Vector3 orig = OriginalMesh.Vertices[i];
                Ray3 ray = new Ray3(vert, -CurrentMesh.VertexNormals[i]);
                int elementIndex;
                float distance;
                if (colVolHierarchy.Intersects(ray, 0, collisionDistanceLimit, out vert, out distance, out elementIndex))
                {
                    var vec = vert - altVert;
                    Vector3 value = vert + (vec.Normalized * (vec.Length * collisionOffsetPercentage));
                    vertices[i] = value;
                    cumulativeDistance += (altVert - value).Length;
                    colVecs.Add(value);
                    protectedVerts[i] = true;
                }
                else
                {
                    if (altVert.NearEquals(orig, 0.1f))
                    {
                        vertices[i] = orig;
                        protectedVerts[i] = false;
                    }
                    else
                        vertices[i] = altVert + (orig - altVert) * returnSpeed;
                    noColIndices.Add(i);
                }
            }

            if (noColIndices.Count > 0 && noColIndices.Count < CurrentMesh.Vertices.Count)
            {
                float cumDistAvg = volumeAmplifier * cumulativeDistance / noColIndices.Count;

                for (int i = 0; i < noColIndices.Count; i++)
                {
                    Vector3 curVert = vertices[i];
                    Vector3 closestPoint = colVecs.OrderBy(x => (x - curVert).LengthSquared).First();
                    float dist = (curVert - closestPoint).Length;

                    if (dist < bulgeDistance && !protectedVerts[i])
                        vertices[i] += OriginalMesh.VertexNormals[i] * ((1 - dist / bulgeDistance) * bulgeMaxHeight);
                    else
                        vertices[i] += OriginalMesh.VertexNormals[i] * cumDistAvg;
                }
            }

            CurrentMesh = new PolygonMesh3(vertices, CurrentMesh.IndexPolygons);

            return CurrentMesh;
        }
    }
}
