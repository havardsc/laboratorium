﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ephere.Geometry;

namespace HavardscLaboratorium.Deformer
{
    public class MeshDefaults
    {
        float currentTime;

        public PolygonMesh3 OriginalMesh { get; protected set; }
        public PolygonMesh3 CurrentMesh { get; set; }
        public float StartTime { get; private set; }
        public float CurrentTime
        {
            get { return currentTime; }
            set
            {
                currentTime = value;
                Update();
            }
        }

        public MeshDefaults(float startTime)
        {
            StartTime = startTime;
        }

        void Update()
        {
            if (OriginalMesh == null)
                OriginalMesh = CurrentMesh;

            if (CurrentMesh == null)
                CurrentMesh = OriginalMesh;

            if (currentTime <= StartTime)
                CurrentMesh = OriginalMesh;
        }
    }
}
