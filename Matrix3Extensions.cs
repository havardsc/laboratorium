﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ephere.Geometry;

namespace HavardscLaboratorium
{
    static class Matrix3Extensions
    {
        public static Matrix3 Add(this Matrix3 m, Matrix3 n)
        {
            Matrix3 sum = new Matrix3();
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                {
                    sum[i, j] = m[i, j] + n[i, j];
                }

            return sum;
        }

        public static Matrix3 Subtract(this Matrix3 m, Matrix3 n)
        {
            Matrix3 diff = new Matrix3();
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    diff[i, j] = m[i, j] - n[i, j];

            return diff;
        }

        public static float Determinant(this Matrix3 m)
        {
            float fCofactor00 = m[1, 1] * m[2, 2] - m[1, 2] * m[2, 1];
            float fCofactor10 = m[1, 2] * m[2, 0] - m[1, 0] * m[2, 2];
            float fCofactor20 = m[1, 0] * m[2, 1] - m[1, 1] * m[2, 0];

            float fDet =
                m[0, 0] * fCofactor00 +
                m[0, 1] * fCofactor10 +
                m[0, 2] * fCofactor20;

            return fDet;
        }

        public static Matrix3 MultiplyScalar(this Matrix3 m, float s)
        {
            Matrix3 newM = new Matrix3();
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    newM[i, j] = m[i, j] * s;

            return newM;
        }

        public static Matrix3 Inverse(this Matrix3 m)
        {
            Matrix3 kInverse = Matrix3.Zero;
            Inverse(m, ref kInverse, 1e-06f);
            return kInverse;
        }

        static bool Inverse(Matrix3 m, ref Matrix3 rkInverse, float fTolerance)
        {
            // Invert a 3x3 using cofactors.  This is about 8 times faster than
            // the Numerical Recipes code which uses Gaussian elimination.

            rkInverse[0, 0] =
                m[1, 1] * m[2, 2] -
                m[1, 2] * m[2, 1];
            rkInverse[0, 1] = 
                m[0, 2] * m[2, 1] -
                m[0, 1] * m[2, 2];
            rkInverse[0, 2] = 
                m[0, 1] * m[1, 2] -
                m[0, 2] * m[1, 1];
            rkInverse[1, 0] = 
                m[1, 2] * m[2, 0] -
                m[1, 0] * m[2, 2];
            rkInverse[1, 1] =
                m[0, 0] * m[2, 2] -
                m[0, 2] * m[2, 0];
            rkInverse[1, 2] = 
                m[0, 2] * m[1, 0] -
                m[0, 0] * m[1, 2];
            rkInverse[2, 0] = 
                m[1, 0] * m[2, 1] -
                m[1, 1] * m[2, 0];
            rkInverse[2, 1] = 
                m[0, 1] * m[2, 0] -
                m[0, 0] * m[2, 1];
            rkInverse[2, 2] = 
                m[0, 0] * m[1, 1] -
                m[0, 1] * m[1, 0];

            float fDet =
                m[0, 0] * rkInverse[0, 0] +
                m[0, 1] * rkInverse[1, 0] +
                m[0, 2] * rkInverse[2, 0];

            if (Math.Abs(fDet) <= fTolerance)
                return false;

            float fInvDet = 1.0f / fDet;
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    rkInverse[i, j] = fInvDet;

            return true;
        }

        public static Matrix3 MultiplyWithTranspose(Vector3 a, Vector3 b)
        {
            Matrix3 m = new Matrix3();
            m[0, 0] = a.x * b.x;
            m[0, 1] = a.x * b.y;
            m[0, 2] = a.x * b.z;

            m[1, 0] = a.y * b.x;
            m[1, 1] = a.y * b.y;
            m[1, 2] = a.y * b.z;

            m[2, 0] = a.z * b.x;
            m[2, 1] = a.z * b.y;
            m[2, 2] = a.z * b.z;

            return m;
        }

        public static float[] Matrix3ToFloatArr(Matrix3 a)
        {
            float[] am = new float[9];
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    am[i * 3 + j] = a[i, j];
            return am;
        }

        public static void FloatArrToMatrix3(float[] arr, ref Matrix3 m)
        {
            for (int i = 0; i < 3; i++)
                for (int j = 0; j < 3; j++)
                    m[i, j] = arr[i * 3 + j];
        }

        public static void Jacobi3(ref Matrix3 a, ref float[] d, ref Matrix3 v)
        {
            float[] af = Matrix3ToFloatArr(a);
            float[] vf = Matrix3ToFloatArr(v);
            Jacobi(3, ref af, ref d, ref vf);
            FloatArrToMatrix3(af, ref a);
            FloatArrToMatrix3(vf, ref v);
        }

        static void Jacobi(int n, ref float[] a, ref float[] d, ref float[] v)
        {
            const int MaxSweeps = 50;
            const float Tolerance = 1e-3f;

            // Set v to the identity matrix, set the vector d to contain the 
            // diagonal elements of the matrix a
            d[0] = a[0];
            d[1] = a[4];
            d[2] = a[8];

            for (int l = 1; l < MaxSweeps; l++)
            {
                // Set dnorm to be the maximum norm of the diagonal elements, set
                // onorm to the maximum norm of the off-diagonal elements
                float dnorm = (float)Math.Abs(d[0]) + (float)Math.Abs(d[1]) + (float)Math.Abs(d[2]);
                float onorm = (float)Math.Abs(a[1]) + (float)Math.Abs(a[2]) + (float)Math.Abs(a[5]);

                // Normal end point of this algorithm.
                if (onorm / dnorm <= Tolerance)
                    return;

                for (int j = 1; j < n; j++)
                    for (int i = 0; i <= j - 1; i++)
                    {
                        float b = a[n * i + j];
                        if (Math.Abs(b) > 0.0f)
                        {
                            float t;
                            float dma = d[j] - d[i];
                            if ((Math.Abs(dma) + Math.Abs(b)) <= Math.Abs(dma))
                                t = b / dma;
                            else
                            {
                                float q = 0.5f * dma / b;
                                t = 1.0f / ((float)Math.Abs(q) + (float)Math.Sqrt(1.0f + q * q));
                                if (q < 0.0f)
                                    t = -t;
                            }

                            float c = 1.0f / (float)Math.Sqrt(t * t + 1.0f);
                            float s = t * c;
                            a[n * i + j] = 0.0f;

                            for (int k = 0; k <= i - 1; k++)
                            {
                                float atemp = c * a[n * k + i] - s * a[n * k + j];
                                a[n * k + j] = s * a[n * k + i] + c * a[n * k + j];
                                a[n * k + i] = atemp;
                            }

                            for (int k = i + 1; k <= j - 1; k++)
                            {
                                float atemp = c * a[n * i + k] - s * a[n * k + j];
                                a[n * k + j] = s * a[n * i + k] + c * a[n * k + j];
                                a[n * i + k] = atemp;
                            }

                            for (int k = j + 1; k < n; k++)
                            {
                                float atemp = c * a[n * i + k] - s * a[n * j + k];
                                a[n * j + k] = s * a[n * i + k] + c * a[n * j + k];
                                a[n * i + k] = atemp;
                            }

                            for (int k = 0; k < n; k++)
                            {
                                float vtemp = c * v[n * k + i] - s * v[n * k + j];
                                v[n * k + j] = s * v[n * k + i] + c * v[n * k + j];
                                v[n * k + i] = vtemp;
                            }

                            float dtemp = c * c * d[i] + s * s * d[j] - 2.0f * c * s * b;
                            d[j] = s * s * d[i] + c * c * d[j] + 2.0f * c * s * b;
                            d[i] = dtemp;
                        }
                    }
            }
        }
    }
}
