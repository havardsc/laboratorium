﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ephere.Geometry;

namespace HavardscLaboratorium.MarchingCubes
{
    static class Utils
    {
        public static int TriangulateField(
            float threshold, 
            float[] scalarField, 
            Vector3[] normalField, 
            Vector3 resolution, 
            Vector3 cubeSize, 
            List<Vector3> outputVertices, 
            List<Vector3> outputNormals)
        {
            int vertexCount = 0;
            int resolutionZ2 = (int)(resolution.y * resolution.x);

            int z = 0;
            int iOffset0 = 0;
            int iOffset1 = resolutionZ2;

            for (int i = 0; i < resolution.z - 1; i++)
            {
                int y = 0;
                int jOffset0 = 0;
                int jOffset1 = (int)resolution.x;

                for (int j = 0; j < resolution.y - 1; j++)
                {
                    int x = 0;
                    int kOffset0 = 0;
                    int kOffset1 = 1;

                    for (int k = 0; k < resolution.x - 1; k++)
                    {
                        float[] values = new float[8];
                        values[0] = scalarField[kOffset0 + jOffset0 + iOffset0];
                        values[1] = scalarField[kOffset1 + jOffset0 + iOffset0];
                        values[2] = scalarField[kOffset1 + jOffset0 + iOffset1];
                        values[3] = scalarField[kOffset0 + jOffset0 + iOffset1];
                        values[4] = scalarField[kOffset0 + jOffset1 + iOffset0];
                        values[5] = scalarField[kOffset1 + jOffset1 + iOffset0];
                        values[6] = scalarField[kOffset1 + jOffset1 + iOffset1];
                        values[7] = scalarField[kOffset0 + jOffset1 + iOffset1];

                        Vector3[] normals = new Vector3[8];
                        normals[0] = normalField[kOffset0 + jOffset0 + iOffset0].Normalized;
                        normals[1] = normalField[kOffset1 + jOffset0 + iOffset0].Normalized;
                        normals[2] = normalField[kOffset1 + jOffset0 + iOffset1].Normalized;
                        normals[3] = normalField[kOffset0 + jOffset0 + iOffset1].Normalized;
                        normals[4] = normalField[kOffset0 + jOffset1 + iOffset0].Normalized;
                        normals[5] = normalField[kOffset1 + jOffset1 + iOffset0].Normalized;
                        normals[6] = normalField[kOffset1 + jOffset1 + iOffset1].Normalized;
                        normals[7] = normalField[kOffset0 + jOffset1 + iOffset1].Normalized;

                        vertexCount += TriangulateCube(threshold, values, normals, new Vector3(x, y, z), cubeSize, ref outputVertices, ref outputNormals, vertexCount);

                        x += (int)cubeSize.x;
                        kOffset0 = kOffset1;
                        kOffset1++;
                    }

                    y += (int)cubeSize.y;
                    jOffset0 = jOffset1;
                    jOffset1 += (int)resolution.x;
                }

                z += (int)cubeSize.z;
                iOffset0 = iOffset1;
                iOffset1 += (int)resolutionZ2;
            }

            return vertexCount;
        }

        static int TriangulateCube(float threshold, float[] values, Vector3[] inputNormals, Vector3 position, Vector3 size, ref List<Vector3> outputVertices, ref List<Vector3> outputNormals, int index)
        {
            int cubeIndex = 0;
            if (values[0] < threshold) cubeIndex |= 1;
            if (values[1] < threshold) cubeIndex |= 2;
            if (values[2] < threshold) cubeIndex |= 4;
            if (values[3] < threshold) cubeIndex |= 8;
            if (values[4] < threshold) cubeIndex |= 16;
            if (values[5] < threshold) cubeIndex |= 32;
            if (values[6] < threshold) cubeIndex |= 64;
            if (values[7] < threshold) cubeIndex |= 128;
            if (EdgeTable[cubeIndex] == 0) return 0;


            Vector3 c0 = new Vector3(position.x, position.y, position.z);
            Vector3 c1 = new Vector3(position.x + size.x, position.y + size.y, position.z + size.z);
            Vector3 p0 = new Vector3(c0.x, c0.y, c0.z);
            Vector3 p1 = new Vector3(c1.x, c0.y, c0.z);
            Vector3 p2 = new Vector3(c1.x, c0.y, c1.z);
            Vector3 p3 = new Vector3(c0.x, c0.y, c1.z);
            Vector3 p4 = new Vector3(c0.x, c1.y, c0.z);
            Vector3 p5 = new Vector3(c1.x, c1.y, c0.z);
            Vector3 p6 = new Vector3(c1.x, c1.y, c1.z);
            Vector3 p7 = new Vector3(c0.x, c1.y, c1.z);


            Vector3[] vertices = new Vector3[12];
            Vector3[] normals = new Vector3[12];
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 1)) Lerp(p0, p1, inputNormals[0], inputNormals[1], values[0], values[1], threshold, ref vertices[0], ref normals[0]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 2)) Lerp(p1, p2, inputNormals[1], inputNormals[2], values[1], values[2], threshold, ref vertices[1], ref normals[1]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 4)) Lerp(p2, p3, inputNormals[2], inputNormals[3], values[2], values[3], threshold, ref vertices[2], ref normals[2]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 8)) Lerp(p3, p0, inputNormals[3], inputNormals[0], values[3], values[0], threshold, ref vertices[3], ref normals[3]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 16)) Lerp(p4, p5, inputNormals[4], inputNormals[5], values[4], values[5], threshold, ref vertices[4], ref normals[4]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 32)) Lerp(p5, p6, inputNormals[5], inputNormals[6], values[5], values[6], threshold, ref vertices[5], ref normals[5]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 64)) Lerp(p6, p7, inputNormals[6], inputNormals[7], values[6], values[7], threshold, ref vertices[6], ref normals[6]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 128)) Lerp(p7, p4, inputNormals[7], inputNormals[4], values[7], values[4], threshold, ref vertices[7], ref normals[7]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 256)) Lerp(p0, p4, inputNormals[0], inputNormals[4], values[0], values[4], threshold, ref vertices[8], ref normals[8]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 512)) Lerp(p1, p5, inputNormals[1], inputNormals[5], values[1], values[5], threshold, ref vertices[9], ref normals[9]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 1024)) Lerp(p2, p6, inputNormals[2], inputNormals[6], values[2], values[6], threshold, ref vertices[10], ref normals[10]);
            if (Convert.ToBoolean(EdgeTable[cubeIndex] & 2048)) Lerp(p3, p7, inputNormals[3], inputNormals[7], values[3], values[7], threshold, ref vertices[11], ref normals[11]);


            int vertexCount = 0;
            while (TriangleTable[cubeIndex, vertexCount] != -1)
            {
                int a = TriangleTable[cubeIndex, vertexCount + 0];
                int b = TriangleTable[cubeIndex, vertexCount + 1];
                int c = TriangleTable[cubeIndex, vertexCount + 2];

                outputVertices.Add(vertices[a]);
                outputVertices.Add(vertices[b]);
                outputVertices.Add(vertices[c]);

                outputNormals.Add(normals[a]);
                outputNormals.Add(normals[b]);
                outputNormals.Add(normals[c]);

                index += 3;
                vertexCount += 3;
            }

            return vertexCount;
        }


        public static void DeriveField(float[] scalarField, Vector3 resolution, ref Vector3[] outputNormalField)
        {
            int resolutionZ2 = (int)(resolution.x * resolution.y);

            int iOffset0 = 0;
            int iOffset1 = resolutionZ2;
            int iOffset2 = 2 * resolutionZ2;

            for (int i = 0; i < resolution.z - 2; i++)
            {
                int jOffset0 = 0;
                int jOffset1 = (int)resolution.x;
                int jOffset2 = (int)(2 * resolution.x);

                for (int j = 0; j < resolution.y - 2; j++)
                {
                    int kOffset0 = 0;
                    int kOffset1 = 1;
                    int kOffset2 = 2;

                    for (int k = 0; k < resolution.x - 2; k++)
                    {
                        float x0 = scalarField[kOffset0 + jOffset1 + iOffset1];
                        float y0 = scalarField[kOffset1 + jOffset0 + iOffset1];
                        float z0 = scalarField[kOffset1 + jOffset1 + iOffset0];

                        float x1 = scalarField[kOffset2 + jOffset1 + iOffset1];
                        float y1 = scalarField[kOffset1 + jOffset2 + iOffset1];
                        float z1 = scalarField[kOffset1 + jOffset1 + iOffset2];

                        outputNormalField[kOffset1 + jOffset1 + iOffset1] = new Vector3(x1 - x0, y1 - y0, z1 - z0);

                        kOffset0 = kOffset1;
                        kOffset1 = kOffset2;
                        kOffset2++;
                    }

                    jOffset0 = jOffset1;
                    jOffset1 = jOffset2;
                    jOffset2 += (int)resolution.x;
                }

                iOffset0 = iOffset1;
                iOffset1 = iOffset2;
                iOffset2 += (int)resolutionZ2;
            }
        }

        static void Lerp(Vector3 p0, Vector3 p1, Vector3 n0, Vector3 n1, float v0, float v1, float threshold, ref Vector3 p, ref Vector3 n)
        {
            if (v0 == v1)
            {
                p = p0;
                n = n0;
                return;
            }

            float f = (float)(threshold - v0) / (float)(v1 - v0);
            p.x = p0.x + (p1.x - p0.x) * f;
            p.y = p0.y + (p1.y - p0.y) * f;
            p.z = p0.z + (p1.z - p0.z) * f;
            n.x = n0.x + (n1.x - n0.x) * f;
            n.y = n0.y + (n1.y - n0.y) * f;
            n.z = n0.z + (n1.z - n0.z) * f;
        }
        
        /*
        static void Lerp(
            Vector3 p0, 
            Vector3 p1, 
            Vector3 n0, 
            Vector3 n1, 
            int v0, 
            int v1, 
            int threshold, 
            ref Vector3 p, 
            ref Vector3 n)
	    {
		    if (v0 == v1)
		    {
			    p = p0;
			    n = n0;
			    return;
		    }
		    float f = (float)(threshold - v0) / (float)(v1 - v0);
		    p.x = p0.x + (p1.x - p0.x) * f;
		    p.y = p0.y + (p1.y - p0.y) * f;
		    p.z = p0.z + (p1.z - p0.z) * f;
		    n.x = n0.x + (n1.x - n0.x) * f;
		    n.y = n0.y + (n1.y - n0.y) * f;
		    n.z = n0.z + (n1.z - n0.z) * f;
	    }


        static int TriangulateCube(
            int threshold, 
            int[] values, 
            Vector3[] inputNormals, 
            Vector3 position, 
            Vector3 size, 
            List<Vector3> outputVertices, 
            List<Vector3> outputNormals,
            int index)
	    {
		    int cubeIndex = 0;
		    if (values[0] < threshold) cubeIndex |= 1;
		    if (values[1] < threshold) cubeIndex |= 2;
		    if (values[2] < threshold) cubeIndex |= 4;
		    if (values[3] < threshold) cubeIndex |= 8;
		    if (values[4] < threshold) cubeIndex |= 16;
		    if (values[5] < threshold) cubeIndex |= 32;
		    if (values[6] < threshold) cubeIndex |= 64;
		    if (values[7] < threshold) cubeIndex |= 128;
		    if (EdgeTable[cubeIndex] == 0) return 0;


		    Vector3 c0 = new Vector3(position.x, position.y, position.z);
		    Vector3 c1 = new Vector3(position.x+size.x, position.y+size.y, position.z+size.z);
		    Vector3 p0 = new Vector3(c0.x, c0.y, c0.z);
		    Vector3 p1 = new Vector3(c1.x, c0.y, c0.z);
		    Vector3 p2 = new Vector3(c1.x, c0.y, c1.z);
		    Vector3 p3 = new Vector3(c0.x, c0.y, c1.z);
		    Vector3 p4 = new Vector3(c0.x, c1.y, c0.z);
		    Vector3 p5 = new Vector3(c1.x, c1.y, c0.z);
		    Vector3 p6 = new Vector3(c1.x, c1.y, c1.z);
		    Vector3 p7 = new Vector3(c0.x, c1.y, c1.z);


		    Vector3[] vertices = new Vector3[12], normals = new Vector3[12];
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 1))    Lerp(p0, p1, inputNormals[0], inputNormals[1], values[0], values[1], threshold, ref vertices[0], ref normals[0]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 2))    Lerp(p1, p2, inputNormals[1], inputNormals[2], values[1], values[2], threshold, ref vertices[1], ref normals[1]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 4))    Lerp(p2, p3, inputNormals[2], inputNormals[3], values[2], values[3], threshold, ref vertices[2], ref normals[2]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 8))    Lerp(p3, p0, inputNormals[3], inputNormals[0], values[3], values[0], threshold, ref vertices[3], ref normals[3]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 16))   Lerp(p4, p5, inputNormals[4], inputNormals[5], values[4], values[5], threshold, ref vertices[4], ref normals[4]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 32))   Lerp(p5, p6, inputNormals[5], inputNormals[6], values[5], values[6], threshold, ref vertices[5], ref normals[5]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 64))   Lerp(p6, p7, inputNormals[6], inputNormals[7], values[6], values[7], threshold, ref vertices[6], ref normals[6]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 128))  Lerp(p7, p4, inputNormals[7], inputNormals[4], values[7], values[4], threshold, ref vertices[7], ref normals[7]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 256))  Lerp(p0, p4, inputNormals[0], inputNormals[4], values[0], values[4], threshold, ref vertices[8], ref normals[8]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 512))  Lerp(p1, p5, inputNormals[1], inputNormals[5], values[1], values[5], threshold, ref vertices[9], ref normals[9]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 1024)) Lerp(p2, p6, inputNormals[2], inputNormals[6], values[2], values[6], threshold, ref vertices[10], ref normals[10]);
		    if (Convert.ToBoolean(EdgeTable[cubeIndex] & 2048)) Lerp(p3, p7, inputNormals[3], inputNormals[7], values[3], values[7], threshold, ref vertices[11], ref normals[11]);


		    int vertexCount = 0;
		    while (TriangleTable[cubeIndex, vertexCount] != -1)
		    {
			    int a = TriangleTable[cubeIndex, vertexCount + 0];
			    int b = TriangleTable[cubeIndex, vertexCount + 1];
			    int c = TriangleTable[cubeIndex, vertexCount + 2];

                outputVertices.Add(vertices[a]);
                outputVertices.Add(vertices[b]);
                outputVertices.Add(vertices[c]);

                outputNormals.Add(normals[a]);
                outputNormals.Add(normals[b]);
                outputNormals.Add(normals[c]);

			    vertexCount += 3;
		    }

		    return vertexCount;
	    }

        public static int TriangulateField(
            int threshold, 
            int[] scalarField, 
            Vector3[] normalField, 
            Vector3 resolution, 
            Vector3 cubeSize, 
            List<Vector3> outputVertices, 
            List<Vector3> outputNormals)
        {
	        int vertexCount = 0;
	        int resolutionZ2 = (int)(resolution.y*resolution.x);

	        int z = 0;
	        int iOffset0 = 0;
	        int iOffset1 = resolutionZ2;
	        for (int i = 0; i < resolution.z-1; i++)
	        {
		        int y = 0;
		        int jOffset0 = 0;
		        int jOffset1 = (int)resolution.x;
		        for (int j = 0; j < resolution.y-1; j++)
		        {
			        int x = 0;
			        int kOffset0 = 0;
			        int kOffset1 = 1;
			        for (int k = 0; k < resolution.x-1; k++)
			        {
				        int[] values = new int[8];
				        values[0] = scalarField[kOffset0 + jOffset0 + iOffset0];
				        values[1] = scalarField[kOffset1 + jOffset0 + iOffset0];
				        values[2] = scalarField[kOffset1 + jOffset0 + iOffset1];
				        values[3] = scalarField[kOffset0 + jOffset0 + iOffset1];
				        values[4] = scalarField[kOffset0 + jOffset1 + iOffset0];
				        values[5] = scalarField[kOffset1 + jOffset1 + iOffset0];
				        values[6] = scalarField[kOffset1 + jOffset1 + iOffset1];
				        values[7] = scalarField[kOffset0 + jOffset1 + iOffset1];
                        
				        Vector3[] normals = new Vector3[8];
				        normals[0] = normalField[kOffset0 + jOffset0 + iOffset0].Normalized;
				        normals[1] = normalField[kOffset1 + jOffset0 + iOffset0].Normalized;
				        normals[2] = normalField[kOffset1 + jOffset0 + iOffset1].Normalized;
				        normals[3] = normalField[kOffset0 + jOffset0 + iOffset1].Normalized;
				        normals[4] = normalField[kOffset0 + jOffset1 + iOffset0].Normalized;
				        normals[5] = normalField[kOffset1 + jOffset1 + iOffset0].Normalized;
				        normals[6] = normalField[kOffset1 + jOffset1 + iOffset1].Normalized;
				        normals[7] = normalField[kOffset0 + jOffset1 + iOffset1].Normalized;

				        vertexCount += TriangulateCube(
                            threshold, 
                            values, 
                            normals, 
                            new Vector3(x,y,z), 
                            cubeSize, 
                            outputVertices, 
                            outputNormals, 
                            vertexCount);

				        x += (int)cubeSize.x;
				        kOffset0 = kOffset1;
				        kOffset1++;
			        }

			        y += (int)cubeSize.y;
			        jOffset0 = jOffset1;
			        jOffset1 += (int)resolution.x;
		        }

		        z += (int)cubeSize.z;
		        iOffset0 = iOffset1;
		        iOffset1 += resolutionZ2;
	        }

	        return vertexCount;
        }

        public static void DeriveField(int[] scalarField, Vector3 resolution, ref Vector3[] outputNormalField)
        {
            int resolutionZ2 = (int)(resolution.x * resolution.y);

            int iOffset0 = 0;
            int iOffset1 = resolutionZ2;
            int iOffset2 = 2 * resolutionZ2;
            for (int i = 0; i < resolution.z - 2; i++)
            {
                int jOffset0 = 0;
                int jOffset1 = (int)resolution.x;
                int jOffset2 = 2 * (int)resolution.x;
                for (int j = 0; j < resolution.y - 2; j++)
                {
                    int kOffset0 = 0;
                    int kOffset1 = 1;
                    int kOffset2 = 2;
                    for (int k = 0; k < resolution.x - 2; k++)
                    {
                        int x0 = scalarField[kOffset0 + jOffset1 + iOffset1];
                        int y0 = scalarField[kOffset1 + jOffset0 + iOffset1];
                        int z0 = scalarField[kOffset1 + jOffset1 + iOffset0];

                        int x1 = scalarField[kOffset2 + jOffset1 + iOffset1];
                        int y1 = scalarField[kOffset1 + jOffset2 + iOffset1];
                        int z1 = scalarField[kOffset1 + jOffset1 + iOffset2];

                        outputNormalField[kOffset1 + jOffset1 + iOffset1] = new Vector3(x1 - x0, y1 - y0, z1 - z0);

                        kOffset0 = kOffset1;
                        kOffset1 = kOffset2;
                        kOffset2++;
                    }
                    jOffset0 = jOffset1;
                    jOffset1 = jOffset2;
                    jOffset2 += (int)resolution.x;
                }
                iOffset0 = iOffset1;
                iOffset1 = iOffset2;
                iOffset2 += resolutionZ2;
            }
        }
        */
        static int[] EdgeTable = new int[256]
        {
	        0x0  , 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c,
	        0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00,
	        0x190, 0x99 , 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c,
	        0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90,
	        0x230, 0x339, 0x33 , 0x13a, 0x636, 0x73f, 0x435, 0x53c,
	        0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30,
	        0x3a0, 0x2a9, 0x1a3, 0xaa , 0x7a6, 0x6af, 0x5a5, 0x4ac,
	        0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0,
	        0x460, 0x569, 0x663, 0x76a, 0x66 , 0x16f, 0x265, 0x36c,
	        0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60,
	        0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff , 0x3f5, 0x2fc,
	        0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0,
	        0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x55 , 0x15c,
	        0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950,
	        0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc ,
	        0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0,
	        0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc,
	        0xcc , 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0,
	        0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c,
	        0x15c, 0x55 , 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650,
	        0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc,
	        0x2fc, 0x3f5, 0xff , 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
	        0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c,
	        0x36c, 0x265, 0x16f, 0x66 , 0x76a, 0x663, 0x569, 0x460,
	        0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac,
	        0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa , 0x1a3, 0x2a9, 0x3a0,
	        0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c,
	        0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x33 , 0x339, 0x230,
	        0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c,
	        0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99 , 0x190,
	        0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c,
	        0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x0   
        };

        static int[,] TriangleTable = new int[256, 16]
        {
	        {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1},
	        {3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1},
	        {3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1},
	        {3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1},
	        {9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1},
	        {9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
	        {2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1},
	        {8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1},
	        {9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
	        {4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1},
	        {3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1},
	        {1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1},
	        {4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1},
	        {4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1},
	        {9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1},
	        {1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
	        {5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1},
	        {2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1},
	        {9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
	        {0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
	        {2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1},
	        {10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1},
	        {4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1},
	        {5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1},
	        {5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1},
	        {9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1},
	        {0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1},
	        {1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1},
	        {10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1},
	        {8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1},
	        {2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1},
	        {7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1},
	        {9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1},
	        {2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1},
	        {11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1},
	        {9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1},
	        {5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1},
	        {11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1},
	        {11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
	        {1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1},
	        {9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1},
	        {5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1},
	        {2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
	        {0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
	        {5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1},
	        {6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1},
	        {0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1},
	        {3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1},
	        {6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1},
	        {5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1},
	        {1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
	        {10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1},
	        {6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1},
	        {1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1},
	        {8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1},
	        {7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1},
	        {3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
	        {5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1},
	        {0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1},
	        {9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1},
	        {8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1},
	        {5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1},
	        {0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1},
	        {6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1},
	        {10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1},
	        {10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1},
	        {8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1},
	        {1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1},
	        {3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1},
	        {0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1},
	        {10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1},
	        {0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1},
	        {3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1},
	        {6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1},
	        {9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1},
	        {8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1},
	        {3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1},
	        {6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1},
	        {0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1},
	        {10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1},
	        {10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1},
	        {1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1},
	        {2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1},
	        {7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1},
	        {7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1},
	        {2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1},
	        {1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1},
	        {11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1},
	        {8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1},
	        {0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1},
	        {7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
	        {10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
	        {2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
	        {6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1},
	        {7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1},
	        {2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1},
	        {1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1},
	        {10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1},
	        {10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1},
	        {0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1},
	        {7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1},
	        {6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1},
	        {8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1},
	        {9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1},
	        {6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1},
	        {4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1},
	        {10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1},
	        {8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1},
	        {0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1},
	        {1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1},
	        {8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1},
	        {10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1},
	        {4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1},
	        {10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
	        {5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
	        {11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1},
	        {9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
	        {6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1},
	        {7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1},
	        {3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1},
	        {7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1},
	        {9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1},
	        {3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1},
	        {6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1},
	        {9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1},
	        {1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1},
	        {4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1},
	        {7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1},
	        {6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1},
	        {3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1},
	        {0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1},
	        {6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1},
	        {0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1},
	        {11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1},
	        {6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1},
	        {5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1},
	        {9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1},
	        {1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1},
	        {1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1},
	        {10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1},
	        {0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1},
	        {5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1},
	        {10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1},
	        {11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1},
	        {9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1},
	        {7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1},
	        {2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1},
	        {8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1},
	        {9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1},
	        {9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1},
	        {1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1},
	        {9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1},
	        {9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1},
	        {5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1},
	        {0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1},
	        {10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1},
	        {2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1},
	        {0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1},
	        {0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1},
	        {9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1},
	        {5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1},
	        {3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1},
	        {5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1},
	        {8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1},
	        {9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1},
	        {0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1},
	        {1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1},
	        {3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1},
	        {4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1},
	        {9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1},
	        {11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1},
	        {11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1},
	        {2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1},
	        {9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1},
	        {3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1},
	        {1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1},
	        {4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1},
	        {4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1},
	        {0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1},
	        {3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1},
	        {3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1},
	        {0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1},
	        {9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1},
	        {1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	        {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}
        };
    }
}
