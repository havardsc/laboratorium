﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ephere.Geometry;

using Autodesk.Max;

namespace HavardscLaboratorium.MarchingCubes
{
    public static class MarchingCubes
    {
        public static PolygonMesh3 Create(int isoValue, int resolution, float cellSize, float[] scalarField)
        {
            Vector3[] normalField = new Vector3[resolution * resolution * resolution];
            //float[] scalarField = NoiseScalarField(t, resolution, scale);//CreateScalarField(resolution, cellSize, size, positions);

            List<Vector3> vertices = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();

            Utils.DeriveField(scalarField, new Vector3(resolution, resolution, resolution), ref normalField);
            
            int vertexCount = Utils.TriangulateField(
                isoValue,
                scalarField,
                normalField,
                new Vector3(resolution, resolution, resolution),
                new Vector3(cellSize, cellSize, cellSize),
                vertices,
                normals);

            List<PolygonMesh3.IndexPolygon> polygons = new List<PolygonMesh3.IndexPolygon>();
            for (int i = 0; i < vertexCount; i += 3)
                polygons.Add(new PolygonMesh3.IndexPolygon(new int[] { i, i + 2, i + 1 }));

            Vector3 resVec = new Vector3(resolution, resolution, resolution) * cellSize * 0.5f;

            return new PolygonMesh3(vertices.Select(i => i - resVec) , polygons);
        }

        public static float[] NoiseScalarField(float t, int resolution, float scale)
        {
            var global = Autodesk.Max.GlobalInterface.Instance;
            int resolution2 = resolution * resolution;
            float[] field = new float[resolution2 * resolution ];           
            
            for (float z = 0; z < resolution; z++)
                for (float y = 0; y < resolution; y++)
                    for (float x = 0; x < resolution; x++)
                        field[(int)(x + y * resolution + z * resolution2)] = global.Noise4(
                            global.Point3.Create(x / resolution, y / resolution, z / resolution).MultiplyBy(scale), t);

            return field;
        }

        public static float[] CreateScalarField(int resolution, float cellSize, int size, Vector3[] positions)
        {
            float gridSize = resolution * cellSize;
            
            Vector3 max = new Vector3(gridSize, gridSize, gridSize) * 0.5f;
            Vector3 min = -max;

            int resolution2 = resolution * resolution;
            float[] scalarField = new float[resolution2 * resolution];

            foreach (Vector3 pos in positions)
            {
                Vector3 adjustedPos = pos - min; //* resolution / gridSize;
                int xIndex = (int)adjustedPos.x;
                int yIndex = (int)adjustedPos.y;
                int zIndex = (int)adjustedPos.z;

                scalarField[resolution2 * zIndex + yIndex * resolution + xIndex] += 100;

                for (int z = Math.Max(zIndex - size, 0); z <= Math.Min(zIndex + size, resolution); z++)
                    for (int y = Math.Max(yIndex - size, 0); y <= Math.Min(yIndex + size, resolution); y++)
                        for (int x = Math.Max(xIndex - size, 0); x <= Math.Min(xIndex + size, resolution); x++)
                        {
                            if (xIndex == x && yIndex == y && zIndex == z)
                                continue;
                            float r = new Vector3(x - xIndex, y - yIndex, z - zIndex).Length;
                            scalarField[resolution2 * z + resolution * y + x] += 100.0f / r;
                        }
            }

            return scalarField;
        }

        public static float[] ScalarFieldFromMesh(float cellSize, float margin, PolygonMesh3 mesh)
        {
            PolygonMesh3ProjectedGrid projGrid = new PolygonMesh3ProjectedGrid(mesh);

            int xRes = (int)((mesh.BoundingBox.Width + margin) / cellSize);
            int yRes = (int)((mesh.BoundingBox.Length + margin) / cellSize);
            int zRes = (int)((mesh.BoundingBox.Height + margin) / cellSize);

            int resolution = Math.Max(Math.Max(xRes, yRes), zRes);
            int resolution2 = resolution * resolution;
            float marginHalf = 0.5f * margin;
            Vector3 min = mesh.BoundingBox.MinimumCorner - new Vector3(marginHalf, marginHalf, marginHalf);

            float[] field = new float[resolution2 * resolution];

            for (int z = 0; z < resolution; z++)
                for (int y = 0; y < resolution; y++)
                    for (int x = 0; x < resolution; x++)
                    {
                        Vector3 pos = new Vector3(x, y, z) + min;
                        if (projGrid.IsInside(pos))
                            field[z * resolution2 + y * resolution + x] = 1;
                        else
                            field[z * resolution2 + y * resolution + x] = 0;
                    }
            return field;
        }

        public static int ResolutionFromMesh(float cellSize, float margin, PolygonMesh3 mesh)
        {
            int xRes = (int)((mesh.BoundingBox.Width + margin) / cellSize);
            int yRes = (int)((mesh.BoundingBox.Length + margin) / cellSize);
            int zRes = (int)((mesh.BoundingBox.Height + margin) / cellSize);

            return Math.Max(Math.Max(xRes, yRes), zRes);
        }
    }
}
