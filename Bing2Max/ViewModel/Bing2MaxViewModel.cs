﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Ephere.Geometry;
using Microsoft.Maps.MapControl.WPF;

namespace HavardscLaboratorium.Bing2Max.ViewModel
{
    public class Bing2MaxViewModel : ViewModelBase
    {
        const string credentials = INSERTYOURKEYHERE
        string sessionBingMapsKey;

        double mapSize;
        public double MapSize
        {
            get { return mapSize; }
            set
            {
                if (mapSize == value) return;
                mapSize = value;
                OnPropertyChanged("MapSize");
            }
        }

        public PolygonMesh3 TextureMesh { get; private set; }

        PolygonMesh3 mesh;
        public PolygonMesh3 Mesh 
        {
            get { return mesh; }
            private set
            {
                mesh = value;
                Bing2Max.Model.BingToMax.MeshGenerated -= MeshFromBingReady;
                OnMeshGenerated();
            }
        }

        string fullTexturePath;
        public string FullTexturePath
        {
            get { return fullTexturePath; }
            set
            {
                fullTexturePath = value;
                OnTextureGenerated();
            }
        }

        public Map MapControl { get; private set; }
        public int NodeHandle { get; set; }

        public event EventHandler MeshGenerated;
        public event EventHandler TextureGenerated;

        public ICommand GenerateMeshCommand { get; private set; }

        public Bing2MaxViewModel(string texturePath)
        {
            mapSize = 800;

            MapControl = new Map()
            {
                CredentialsProvider = new ApplicationIdCredentialsProvider(credentials),
                Mode = new AerialMode(true),
                Height = mapSize,
                Width = mapSize,
                Margin = new System.Windows.Thickness(5)
            };

            MapControl.CredentialsProvider.GetCredentials(
                i => sessionBingMapsKey = i.ApplicationId);

            GenerateMeshCommand = new RelayCommand<object>(_ =>
            {
                Bing2Max.Model.BingToMax.MeshGenerated += MeshFromBingReady;

                BitmapImage map = Bing2Max.Model.BingToMax.CreateTexture(
                    MapControl.Center.Latitude,
                    MapControl.Center.Longitude,
                    Math.Round(MapControl.ZoomLevel),
                    mapSize,
                    sessionBingMapsKey);

                map.DownloadCompleted += (s, e) =>
                {
                    var flippedMap = map.FlipVertically();
                    FullTexturePath = flippedMap.FromUriToPng(texturePath);

                    Bing2Max.Model.BingToMax.CreateMesh(
                        MapControl.Center.Latitude,
                        MapControl.Center.Longitude,
                        Math.Round(MapControl.ZoomLevel),
                        mapSize,
                        sessionBingMapsKey);
                };
            });
        }

        void MeshFromBingReady(object sender, Model.BingToMax.PolygonMesh3EventArgs e)
        {
            TextureMesh = e.TextureMesh;
            Mesh = e.Mesh;
        }

        void OnMeshGenerated()
        {
            EventHandler handler = MeshGenerated;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }

        void OnTextureGenerated()
        {
            EventHandler handler = TextureGenerated;
            if (handler != null)
                handler(this, EventArgs.Empty);
        }
    }
}
