﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using Ephere.Geometry;

namespace HavardscLaboratorium.Bing2Max.Model
{
    static class BingToMax
    {
        public static event EventHandler<PolygonMesh3EventArgs> MeshGenerated = delegate { };

        public static BitmapImage CreateTexture(
            double centerLatitude, 
            double centerLongitude, 
            double zoom, 
            double mapSize, 
            string sessionBingMapsKey)
        {
            string imgUrl = string.Format(
                CultureInfo.InvariantCulture,
                "http://dev.virtualearth.net/REST/v1/Imagery/Map/Aerial/{0},{1}/{2}?mapSize={3},{4}&key={5}",
                centerLatitude,
                centerLongitude,
                zoom,
                mapSize,
                mapSize,
                sessionBingMapsKey);

            return new BitmapImage(new Uri(imgUrl));
        }

        public static void CreateMesh(
            double centerLatitude, 
            double centerLongitude, 
            double zoom,
            double mapSize,
            string sessionBingMapsKey)
        {
            //Only generate models when the user is zoomed in at a decent zoom level, otherwise the model will just be a flat sheet.
            if (zoom < 8)
            {
                MessageBox.Show("This zoom level is not supported. Please zoom in closer (>8).");
                return;
            }

            //calcuate pixel coordinates of center point of map
            double sinLatitudeCenter = Math.Sin(centerLatitude * Math.PI / 180);
            double pixelXCenter = ((centerLongitude + 180) / 360) * 256 * Math.Pow(2, zoom);
            double pixelYCenter = (0.5 - Math.Log((1 + sinLatitudeCenter) / (1 - sinLatitudeCenter)) / (4 * Math.PI)) * 256 * Math.Pow(2, zoom);

            //calculate top left corner pixel coordiates of map image
            double topLeftX = pixelXCenter - (mapSize / 2);
            double topLeftY = pixelYCenter - (mapSize / 2);

            //Calculate bounding coordinates of map view
            double brLongitude, brLatitude, tlLongitude, tlLatitude;

            PixelToLatLong(new System.Windows.Point(/*900*/mapSize, mapSize), out brLatitude, out brLongitude, topLeftX, topLeftY, zoom);
            PixelToLatLong(new System.Windows.Point(0, 0), out tlLatitude, out tlLongitude, topLeftX, topLeftY, zoom);

            //Retrieve elevation data for the specified bounding box
            //Rows * Cols <= 1000 -> Let R = C = 30

            int detail = 30;

            string elevationUrl = string.Format(
                CultureInfo.InvariantCulture,
                "http://dev.virtualearth.net/REST/v1/Elevation/Bounds?bounds={0},{1},{2},{3}&rows=" + detail + "&cols=" + detail + "&key={4}",
                brLatitude,
                tlLongitude,
                tlLatitude,
                brLongitude,
                sessionBingMapsKey);


            WebClient wc = new WebClient();
            wc.OpenReadCompleted += (s, a) =>
            {
                using (Stream stream = a.Result)
                {
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Response));
                    Response response = ser.ReadObject(stream) as Response;

                    if (response != null &&
                        response.ResourceSets != null &&
                        response.ResourceSets.Length > 0 &&
                        response.ResourceSets[0] != null &&
                        response.ResourceSets[0].Resources != null &&
                        response.ResourceSets[0].Resources.Length > 0)
                    {
                        PolygonMesh3 mesh = null;
                        PolygonMesh3 textureMesh = null;


                        ElevationData elevationData = response.ResourceSets[0].Resources[0] as ElevationData;

                        //Map elevation data to 3D Mesh
                        List<Vector3> vertices = new List<Vector3>();
                        List<Vector3> texVertices = new List<Vector3>();

                        List<PolygonMesh3.IndexPolygon> faces = new List<PolygonMesh3.IndexPolygon>();
                        
                        double dLat = Math.Abs(tlLatitude - brLatitude) / detail;
                        double dLon = Math.Abs(tlLongitude - brLongitude) / detail;

                        double x, y, m2p, scale = 100.0;
                        double yMax = (tlLatitude + (detail - 1) - 0.5 * mapSize) / mapSize;

                        for (int r = 0; r < detail; r++)
                        {
                            y = tlLatitude + (dLat * r);

                            for (int c = 0; c < detail; c++)
                            {
                                int idx = r * detail + c;

                                x = tlLongitude + (dLon * c);

                                double z = -elevationData.Elevations[idx];
                                Debug.WriteLine(z);

                                m2p = 156543.04 * Math.Cos(y * Math.PI / 180) / Math.Pow(2, zoom);

                                System.Windows.Point p = LatLongToPixel(y, x, topLeftX, topLeftY, zoom);
                                p.X = scale * (p.X - 0.5 * mapSize) / mapSize;
                                p.Y = scale * (p.Y + 0.5 * mapSize) / mapSize;

                                vertices.Add(new Vector3((float)p.X, (float)p.Y, (float)(-scale * z / mapSize / m2p)));
                                texVertices.Add(new Vector3((float)(0.5 + p.X / scale), (float)(0.5 + p.Y / scale), 0));

                                if (r < detail - 1 && c < detail - 1)
                                {
                                    faces.Add(new PolygonMesh3.IndexPolygon(idx, idx + detail, idx + detail + 1, idx + 1));
                                    //faces.Add(new PolygonMesh3.IndexPolygon(idx, idx + 1, idx + detail + 1, idx + detail));
                                    //faces.Add(new PolygonMesh3.IndexPolygon(idx, idx + 1, idx + detail));
                                    //faces.Add(new PolygonMesh3.IndexPolygon(idx + 1, idx + detail + 1, idx + detail));
                                }
                            }
                        }

                        mesh = new PolygonMesh3(vertices, faces);
                        textureMesh = new PolygonMesh3(texVertices, faces);
                        
                        MeshGenerated(null, new PolygonMesh3EventArgs(mesh, textureMesh));
                    }
                }
            };
            
            wc.OpenReadAsync(new Uri(elevationUrl));
        }

        static System.Windows.Point LatLongToPixel(
            double latitude, 
            double longitude, 
            double topLeftX, 
            double topLeftY, 
            double zoom)
        {
            //Formulas based on following article:
            //http://msdn.microsoft.com/en-us/library/bb259689.aspx

            //calculate pixel coordinate of location
            double sinLatitude = Math.Sin(latitude * Math.PI / 180);
            double pixelX = ((longitude + 180) / 360) * 256 * Math.Pow(2, zoom);
            double pixelY = (0.5 - Math.Log((1 + sinLatitude) / (1 - sinLatitude)) / (4 * Math.PI)) * 256 * Math.Pow(2, zoom);

            //calculate relative pixel cooridnates of location
            double x = pixelX - topLeftX;
            double y = pixelY - topLeftY;

            return new System.Windows.Point((int)Math.Floor(x), (int)Math.Floor(y));
        }

        static void PixelToLatLong(
            System.Windows.Point pixel, 
            out double lat, 
            out double lon, 
            double topLeftX, 
            double topLeftY, 
            double zoom)
        {
            double x = topLeftX + pixel.X;
            double y = topLeftY + pixel.Y;

            lon = (x * 360) / (256 * Math.Pow(2, zoom)) - 180;

            double efactor = Math.Exp((0.5 - y / 256 / Math.Pow(2, zoom)) * 4 * Math.PI);

            lat = Math.Asin((efactor - 1) / (efactor + 1)) * 180 / Math.PI;
        }

        internal class PolygonMesh3EventArgs : EventArgs
        {
            public PolygonMesh3 Mesh { get; private set; }
            public PolygonMesh3 TextureMesh { get; private set; }
            
            public PolygonMesh3EventArgs(PolygonMesh3 mesh, PolygonMesh3 textureMesh)
            {
                Mesh = mesh;
                TextureMesh = textureMesh;
            }
        }
    }
}
