(
	struct Bing2MaxStruct 
	(
		graph,
		meshPlugin,
		custAttDef,
		viewModel,
		
		fn GetCoden =
		(
			dotNetClass "Ephere.Plugins.Coden.Global"
		),

		fn GetLibrary =
		(
			(GetCoden()).Library
		),

		fn GetGraph refTarget =
		(
			testPlugin = (GetCoden()).GetPlugin(Coden_getPluginID refTarget)
			testPlugin.GetGraph(Coden_getInstanceID refTarget)
		),

		fn SetGraph refTarget graph =
		(
			testPlugin = (GetCoden()).GetPlugin(Coden_getPluginID refTarget)
			testPlugin.SetGraph (Coden_getInstanceID refTarget) graph
		),

		fn CreateGraphFromXmlTemplateString xml =
		(
			library = GetLibrary()
			ext = dotNetClass "Ephere.Modeling.Dependency.GraphExtensions"
			ext.CreateGraphFromXmlTemplateString library xml
		),
		
		fn SetInitialGraph =
		(
			graph = CreateGraphFromXmlTemplateString \
			"<?xml version=\"1.0\" encoding=\"utf-8\"?>
			<Templates xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.ephere.com/modeling/dependency/graph/templates.xsd\">
				<Graph Type=\"KulerCamoTexure\">
					<Tags>plugin</Tags>
					<Output Type=\"Ephere.Geometry.PolygonMesh3\" Name=\"output\" Group=\"myOutput\" />
					<Output Type=\"Ephere.Geometry.PolygonMesh3\" Name=\"myOutputTexCoords\" Tags=\"textureCoordinates\" Group=\"myOutput\" />
					<Node Type=\"Ephere.Geometry.PolygonMesh3.create(Ephere.Geometry.PolygonMesh3)\" Name=\"MapPolygonMesh\" />
					<Node Type=\"Ephere.Geometry.PolygonMesh3.create(Ephere.Geometry.PolygonMesh3)\" Name=\"MapTextureMesh\" />
					<Connection From=\"MapPolygonMesh\" Output=\"Output\" Input=\"output\" />
					<Connection From=\"MapTextureMesh\" Output=\"Output\" Input=\"myOutputTexCoords\" />
				</Graph>
			</Templates>"
			
			SetGraph meshPlugin graph
		),
		
		fn SetNodeValue val nodeName inputName =
		(
			graphNode = graph.GetNodeByName nodeName
			if graphNode != undefined and val != undefined then
				(graphNode.GetInput inputName).Value = val
		),
				
		fn OnMeshGenerated s ev = 
		(
			theNode = maxOps.getNodeByHandle s.NodeHandle
			if theNode != undefined then
			(
				nodeAttributes = custAttributes.get theNode 1
				bing2MaxInstance = nodeAttributes.parent 
				bing2MaxInstance.SetNodeValue s.Mesh "MapPolygonMesh" "other"
				bing2MaxInstance.SetNodeValue s.TextureMesh "MapTextureMesh" "other"
			)
		),	
		
		fn OnTextureGenerated s ev = 
		(
			print s.FullTexturePath
			theNode = maxOps.getNodeByHandle s.NodeHandle
			if theNode != undefined then
			(
				mat = standardMaterial \ 
					diffusemap:(bitmapTexture filename:s.FullTexturePath) \
					showInViewport:true
				theNode.material = mat
			)
		),	
		
		fn CreateCustomAttributesDef =
		(
			if custAttDef == undefined then
			(
				custAttDef = attributes bing2maxData
				(
					local view
					local parent

					fn Initialize parent =
					(
						this.parent = parent
					)
					
					rollout guiRollout "Bing2Max"
					(
						button openMapButton "Open map"
						
						on openMapButton pressed do
						(
							view = dotNetObject "HavardscLaboratorium.Bing2Max.View.MapView"
							view.Width = view.Height = 850
							view.Title = "Bing2Max"
							view.DataContext = parent.viewModel
							windowInteropHelper = dotNetObject "System.Windows.Interop.WindowInteropHelper" view
							windowInteropHelper.Owner = (DotNetObject "System.IntPtr" (Windows.GetMaxHWND()))
							view.Show()
						)
					)
				)
			)
			
			custAttributes.add meshPlugin custAttDef
			
			viewModel = dotNetObject "HavardscLaboratorium.Bing2Max.ViewModel.Bing2MaxViewModel" ((getdir #temp) + "\\")
			viewModel.NodeHandle = meshPlugin.handle
			dotNet.AddEventHandler viewModel "MeshGenerated" OnMeshGenerated
			dotNet.AddEventHandler viewModel "TextureGenerated" OnTextureGenerated
			
			(custAttributes.get meshPlugin custAttDef).Initialize this
		),
		
		on Create do
		(
			dotNet.loadAssembly @"C:\Users\h�vard\AppData\Local\Ephere\Library\bin\HavardscLaboratorium.dll" -- insert the correct path here!
			meshPlugin = LabPolygon()
			SetInitialGraph()
			CreateCustomAttributesDef()
		)
	)
	Bing2MaxStruct()
)