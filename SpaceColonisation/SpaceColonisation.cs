﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ephere.Geometry;

namespace HavardscLaboratorium.SpaceColonisation
{
    public class SpaceColonisation
    {
        Random random;
        Point[] points;
        List<Vector3> vertices;
        List<PolylineMesh3.IndexPolyline> branches;
        Node kdTree;
        List<PolylineMesh3.IndexPolyline> deadBranches;
        Vector3[] startPositions;
        float prevTime;

        public float GrowLenght { get; set; }
        public float SearchDistance { get; set; }
        public float DismissLenght { get; set; }
        public int Iterations { get; set; }
        public int MaxNeighbours { get; set; }
        public float StartTime { get; set; }
        public float BranchProbability { get; set; }
        public int Seed { get; set; }

        public static SpaceColonisation CreateOnMesh(
            PolygonMesh3 mesh,
            int numPoints,
            int seed,
            params Vector3[] startPoints)
        {
            var random = new Random(seed);

            //var sp = new Vector3[] { Vector3.Zero, new Vector3(1, 0, 0), new Vector3(0, 1, 0) };

            var points = TriMeshUtils.CreatePointsOnMesh(numPoints, mesh, random);

            var sc = new SpaceColonisation(points, startPoints);
            sc.StartTime = 0.1f;

            return sc;
        }

        public SpaceColonisation(Vector3[] points, Vector3[] startPositions)
        {
            this.startPositions = startPositions;
            this.points = points.Select((i, j) => new Point(i, j)).ToArray();

            kdTree = KDTree.CreateTree(this.points);

            GrowLenght = 1;
            SearchDistance = 5;
            DismissLenght = 2;
            Iterations = 3;
            StartTime = 0;
            BranchProbability = 0;
            MaxNeighbours = 3;
            Seed = 0;
            prevTime = StartTime;
        }

        public PolylineMesh3 Update(float time)
        {
            if (time <= StartTime)
            {
                Cleanup();
                prevTime = time;
                return new PolylineMesh3();
            }
            if (time > prevTime)
            {
                Init();

                for (int i = 0; i < Iterations; i++)
                    for (int j = branches.Count - 1; j > -1; j--)
                    {
                        var branch = branches[j];
                        var curPos = vertices[branch.VertexIndices.Last()];

                        var neighbours = KDTree.RangeSearch(kdTree, curPos, SearchDistance).
                            Where(x => !((Point)x).Visited).ToArray();

                        if (neighbours.Length == 0)
                        {
                            //deadBranches.Add(branch);
                            //branches.Remove(branch);
                        }
                        else
                        {
                            /*var point = neighbours.Count() == 1 ? neighbours.Last() :
                                neighbours.Aggregate(
                                (p0, p1) => ((curPos - p0.Position).Length < (curPos - p1.Position).Length) ? p0 : p1);
                            */
                            var branchPoint = vertices[branch.VertexIndices.Last()];
                            var sortedNeighbours = neighbours.OrderBy(
                                x => (x.Position - branchPoint).LengthSquared).ToArray();

                            MoveTowardsPoints(sortedNeighbours.Take(Math.Min(MaxNeighbours, neighbours.Length)).ToArray(), branch);

                            foreach (var neighbour in neighbours)
                                if ((branchPoint - neighbour.Position).LengthSquared < DismissLenght * DismissLenght)
                                    (neighbour as Point).Visited = true;
                        }
                    }

                var newBranches = new List<PolylineMesh3.IndexPolyline>();

                foreach (var branch in branches)
                    if (random.NextDouble() < BranchProbability)
                        newBranches.Add(new PolylineMesh3.IndexPolyline(new int[] { branch.VertexIndices.Last() }));

                branches.AddRange(newBranches);
                prevTime = time;
            }

            //var faces = new List<PolylineMesh3.IndexPolyline>(branches.Concat(deadBranches));

            //return new PolylineMesh3(vertices, faces);
            return new PolylineMesh3(vertices, branches);
        }

        void Init()
        {
            if (vertices != null)
                return;

            vertices = new List<Vector3>();
            branches = new List<PolylineMesh3.IndexPolyline>();
            deadBranches = new List<PolylineMesh3.IndexPolyline>();
            random = new Random(Seed);

            foreach (var point in points)
                point.Visited = false;

            var index = 0;

            foreach (var startPosition in startPositions)
            {
                vertices.Add(startPosition);
                branches.Add(new PolylineMesh3.IndexPolyline(new int[] { index++ }));
            }
        }

        void Cleanup()
        {
            if (vertices == null)
                return;

            vertices = null;
            branches = null;
            deadBranches = null;
        }

        void MoveTowardsPoints(IPoint[] points, PolylineMesh3.IndexPolyline branch)
        {
            var branchPoint = vertices[branch.VertexIndices.Last()];

            var avg = Vector3.Zero;

            foreach (var point in points)
            {
                var ab = point.Position - branchPoint;
                avg += ab;

                //if (ab.Length < DismissLenght)
                //    (point as Point).Visited = true;
            }

            //avg /= points.Length;
            branch.VertexIndices.Add(vertices.Count);
            vertices.Add(branchPoint + (avg.Normalized * GrowLenght));

        }

        void MoveTowardsPoint(Point point, PolylineMesh3.IndexPolyline branch)
        {
            var branchPoint = vertices[branch.VertexIndices.Last()];
            var dir = point.Position - branchPoint;
            var length = dir.Length;

            Vector3 newPoint;
            if (length > GrowLenght)
                newPoint = branchPoint + (dir / length) * GrowLenght;
            else
            {
                newPoint = point.Position;
                point.Visited = true;
            }

            branch.VertexIndices.Add(vertices.Count);
            vertices.Add(newPoint);
        }
    }
}
