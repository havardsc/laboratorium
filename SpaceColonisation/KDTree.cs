﻿using System;
using System.Collections.Generic;
using System.Linq;

using Ephere.Geometry;

namespace HavardscLaboratorium.SpaceColonisation
{
    public interface IPoint
    {
        Vector3 Position { get; set; }
        int ID { get; set; }
    }

    public class Point : IPoint
    {
        public Vector3 Position { get; set; }
        public bool Visited { get; set; }
        public int ID { get; set; }

        public Point(Vector3 pos, int id)
        {
            Position = pos;
            ID = id;
            Visited = false;
        }
    }

    public class Node
    {
        public Node Left { get; private set; }
        public Node Right { get; private set; }
        public IPoint Point { get; private set; }

        public Node(Node left, Node right, IPoint point)
        {
            Left = left;
            Right = right;
            Point = point;
        }
    }

    public static class KDTree
    {
        public static Node CreateTree(IPoint[] points)
        {
            return CreateNodes(points, 0);
        }

        static Node CreateNodes(IPoint[] points, int depth)
        {
            if (points.Length < 1)
                return null;

            var axis = depth % 3;
            Array.Sort(points, delegate(IPoint p1, IPoint p2) { return p1.Position[axis].CompareTo(p2.Position[axis]); });

            var median = points.Length / 2;

            return new Node(
                CreateNodes(points.Take(median).ToArray(), depth + 1),
                CreateNodes(points.Skip(median + 1).ToArray(), depth + 1),
                points[median]);
        }

        public static List<IPoint> RangeSearch(Node node, Vector3 point, float range)
        {
            var list = new List<IPoint>();
            RangeSearch(node, point, range, list, 0);
            return list;
        }

        static void RangeSearch(Node node, Vector3 point, float range, List<IPoint> points, int depth)
        {
            if (node == null)
                return;

            var axis = depth % 3;
            var position = node.Point.Position;
            var distance = (position - point).Length;

            var posAx = position[axis];
            var pointAx = point[axis];

            if (distance != 0 && distance <= range)
                points.Add(node.Point);

            var nearChild = posAx.CompareTo(pointAx) < 0 ? node.Right : node.Left;

            if (nearChild != null)
                RangeSearch(nearChild, point, range, points, depth + 1);

            if (range.CompareTo(Math.Abs(posAx - pointAx)) > 0)
            {
                var farChild = nearChild == node.Left ? node.Right : node.Left;

                if (farChild != null)
                    RangeSearch(farChild, point, range, points, depth + 1);
            }
        }
    }
}
