﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autodesk.Max;
using Ephere.Geometry;

namespace HavardscLaboratorium.SpaceColonisation
{
    public class SplineMesher
    {
        static IGlobal global = GlobalInterface.Instance;
        List<PolygonMesh3> meshes;
        int prevSides;
        float prevTime;
        float prevStartRadius;

        public int Sides { get; set; }
        public float StartRadius { get; set; }
        public float MaxRadius { get; set; }
        public float GrowthRate { get; set; }
        public float StartTime { get; private set; }

        public SplineMesher(float startTime)
        {
            StartRadius = 1;
            Sides = 3;
            MaxRadius = 4;
            GrowthRate = 1.1f;

            StartTime = startTime;
            prevTime = startTime;

            prevSides = Sides;
            prevStartRadius = StartRadius;
            meshes = new List<PolygonMesh3>();
        }

        public static PolylineMesh3 CreateLine(int segments)
        {
            var vertices = new Vector3[segments];
            var indices = new int[segments];

            for (int i = 0; i < segments; i++)
            {
                vertices[i] = new Vector3(0, 0, i * 10);
                indices[i] = i;
            }
            var line = new PolylineMesh3(vertices,
                new PolylineMesh3.IndexPolyline[] { new PolylineMesh3.IndexPolyline(indices) });

            return line;
        }

        public static PolylineMesh3 CreateCornerLine()
        {
            var v = new Vector3[] {new Vector3(0, 0, 0),
                new Vector3(10, 0, 0),
                new Vector3(10, 10, 0),
                new Vector3(10, 10, 10),
                new Vector3(20, 10, 10),
                new Vector3(20, 00, 10)};

            return new PolylineMesh3(v,
                new PolylineMesh3.IndexPolyline[] { new PolylineMesh3.IndexPolyline(new int[] { 0, 1, 2, 3, 4, 5 }) });
        }

        public PolygonMesh3 CreateTexCoords(PolygonMesh3 mesh)
        {
            List<Vector3> vertices = new List<Vector3>();
            int y = 1;
            for (int i = 0; i < mesh.Vertices.Count; i++)
            {
                int step = i % Sides;
                y = step == 0 ? 1 - y % 2 : y;
                float x = (float)step / Sides;
                vertices.Add(new Vector3(x, y, 1));
            }

            return new PolygonMesh3(vertices, mesh.IndexPolygons);
        }

        public PolygonMesh3 Update(PolylineMesh3 polyline, float time)
        {
            if (time <= StartTime || Sides != prevSides || StartRadius != prevStartRadius)
            {
                for (int i = 0; i < meshes.Count; i++)
                    meshes[i] = new PolygonMesh3();

                prevSides = Sides;
                prevStartRadius = StartRadius;
                prevTime = time;
            }

            if (time <= prevTime)
                return PolygonMesh3Extensions.Combine(meshes);

            if (polyline.IndexPolylines.Count > meshes.Count)
                meshes.AddRange(
                    Enumerable.Range(0, polyline.IndexPolylines.Count - meshes.Count).
                    Select(_ => new PolygonMesh3()));

            else if (polyline.IndexPolylines.Count == 0)
                return new PolygonMesh3();
            else
                meshes.RemoveRange(polyline.IndexPolylines.Count, meshes.Count - polyline.IndexPolylines.Count);

            for (int i = 0; i < meshes.Count; i++)
                BuildMesh(meshes[i], polyline.IndexPolylines[i], polyline.Vertices);

            return PolygonMesh3Extensions.Combine(meshes);
        }

        PolygonMesh3 FixMesh(PolygonMesh3 mesh, float stepLength)
        {
            for (int i = mesh.IndexPolygons.Count - 1; i >= 0; i--)
            {
                var poly = mesh.IndexPolygons[i];
                if ((mesh.Vertices[poly.VertexIndices[0]] - mesh.Vertices[poly.VertexIndices[3]]).LengthSquared > stepLength + MaxRadius * MaxRadius + 3)
                    mesh.IndexPolygons.RemoveAt(i);
            }
            return mesh;
        }

        void BuildMesh(PolygonMesh3 mesh, PolylineMesh3.IndexPolyline line, List<Vector3> vertices)
        {
            if (line.VertexIndices.Count < 2)
                return;

            var vDiff = (int)((Sides * line.VertexIndices.Count - mesh.Vertices.Count) / Sides);

            if (vDiff <= 0)
                return;

            for (int i = vDiff; i > 1; i--)
            {
                var index = line.VertexIndices.Count - i;
                var curPoint = vertices[line.VertexIndices[index]];
                var prevPoint = vertices[line.VertexIndices[index > 0 ? index - 1 : index]];
                var nextPoint = vertices[line.VertexIndices[index + 1]];

                var dir = nextPoint - prevPoint;
                var angStep = 2 * Math.PI / Sides;

                var rm = Xform3.LookAt(curPoint, dir.Normalized, Vector3.ZAxis);


                for (int j = 0; j < Sides; j++)
                {
                    var ang = j * angStep;
                    var vertex = new Vector3(-(float)Math.Cos(ang), -(float)Math.Sin(ang), 0) * StartRadius;
                    vertex = rm * vertex;
                    mesh.Vertices.Add(vertex);

                    if (index == 0)
                        continue;

                    var v = mesh.Vertices.Count - Sides - 1;

                    mesh.IndexPolygons.Add(new PolygonMesh3.IndexPolygon(
                        j != Sides - 1 ?
                        new int[] { v, v + 1, v + Sides + 1, v + Sides } :
                        new int[] { v, v + 1 - Sides, v + 1, v + Sides }));
                }
            }

            ScaleMesh(mesh);
        }

        void ScaleMesh(PolygonMesh3 mesh)
        {
            if (GrowthRate == 1)
                return;

            for (int i = 0; i < mesh.Vertices.Count; i += Sides)
            {
                var center = Vector3.Zero;
                for (int j = 0; j < Sides; j++)
                    center += mesh.Vertices[i + j];

                center /= Sides;

                if ((mesh.Vertices[i] - center).Length > MaxRadius)
                    continue;

                for (int j = 0; j < Sides; j++)
                    mesh.Vertices[i + j] = center + (mesh.Vertices[i + j] - center) * GrowthRate;
            }
        }
    }
}
