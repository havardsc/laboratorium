﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ephere.Geometry;

namespace HavardscLaboratorium.SpaceColonisation
{

    public static class TriMeshUtils
    {
        public static Vector3[] CreatePointInsideMesh(int numPoints, PolygonMesh3 mesh, Random random, int attempts = 1000)
        {
            var prjGrid = new PolygonMesh3ProjectedGrid(mesh);
            var curAttempts = 0;
            var points = new List<Vector3>();

            while (curAttempts < attempts && points.Count < numPoints)
            {
                var newPoint =
                    mesh.BoundingBox.MinimumCorner +
                    Ephere.Geometry.Vector3Tools.GetNextVector3(random) * mesh.BoundingBox.Size;

                if (prjGrid.IsInside(newPoint))
                    points.Add(newPoint);
                else
                    attempts++;
            }

            return points.ToArray();
        }

        public static Vector3[] CreatePointsOnMesh(int numPoints, PolygonMesh3 mesh, Random random)
        {
            var trimesh = new TriangleMesh3(mesh);
            var smallestArea = float.MaxValue;
            var areas = CalculateTriangleAreas(trimesh, ref smallestArea);
            var propArea = areas.Select(x => (int)Math.Ceiling(x / smallestArea)).ToArray();
            var repeatIndex = new List<int>();

            for (int i = 0; i < propArea.Length; i++)
                for (int j = 0; j < propArea[i]; j++)
                    repeatIndex.Add((int)i);

            var points = new Vector3[numPoints];
            for (int i = 0; i < numPoints; i++)
            {
                var index = repeatIndex[(int)(random.NextDouble() * repeatIndex.Count)];
                var face = trimesh.IndexTriangles[index];
                var u = (float)random.NextDouble();
                var v = (float)random.NextDouble();
                if (u + v > 1)
                {
                    u = 1 - u;
                    v = 1 - v;
                }
                var w = 1 - (u + v);
                points[i] =
                    trimesh.Vertices[face.component1] * u +
                    trimesh.Vertices[face.component2] * v +
                    trimesh.Vertices[face.component3] * w;
            }

            return points;
        }

        static float[] CalculateTriangleAreas(TriangleMesh3 trimesh, ref float smallestArea)
        {
            var areas = new float[trimesh.IndexTriangles.Count];
            var index = 0;
            foreach (var t in trimesh.IndexTriangles)
            {
                var a = trimesh.Vertices[t.component1];
                var b = trimesh.Vertices[t.component2];
                var c = trimesh.Vertices[t.component3];
                var ab = a - b;
                var ac = a - c;
                var area = Cross(ab, ac).Length * 0.5f;
                areas[index++] = area;

                if (area < smallestArea)
                    smallestArea = area;
            }

            return areas;
        }

        static Vector3 Cross(Vector3 a, Vector3 b)
        {
            return new Vector3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
        }

        static float Dot(Vector3 a, Vector3 b)
        {
            var d = a[0] * b[0];
            for (int i = 1; i < 3; ++i)
                d += a[i] * b[i];

            return d;
        }
    }
}
