﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace HavardscLaboratorium
{
    static class BitmapSourceUtils
    {
        public static string FromUriToPng(this BitmapSource bmi, string path)
        {
            string fullPath = path + Guid.NewGuid() + ".png";

            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bmi));

            using (FileStream stream = new FileStream(fullPath, FileMode.Create))
            {
                encoder.Save(stream);
            }

            return fullPath;
        }

        public static TransformedBitmap FlipVertically(this BitmapSource bmi)
        {
            TransformedBitmap tb = new TransformedBitmap();
            tb.BeginInit();
            tb.Source = bmi;
            tb.Transform = new ScaleTransform(1, -1, 0, 0);
            tb.EndInit();
            
            return tb;
        }
    }
}
