﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Ephere.Geometry;

namespace HavardscLaboratorium
{
    public static class Vector3Extensions
    {
        public static Vector3 ComponentMultiply(this Vector3 a, Vector3 b)
        {
            return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
        }

        public static float Dot(Vector3 a, Vector3 b)
        {
            var d = a[0] * b[0];
            for (int i = 1; i < 3; ++i)
                d += a[i] * b[i];

            return d;
        }

        public static Vector3 Cross(Vector3 a, Vector3 b)
        {
            return new Vector3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
        }


        public static bool Comparer(this Vector3 a, Vector3 b)
        {
            return a.x < b.x && a.y < b.y && a.z < b.z;
        }
    }
}
